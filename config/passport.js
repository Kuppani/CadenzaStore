// const AnonymousStrategy = require('passport-anonymous').Strategy;
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const randomstring = require("randomstring");

const config = require('../config/config');
const sendMail = require('../methods/email');

const mailgun = require('mailgun-js')({
	apiKey: config.API_KEY_MAILGUN,
	domain: config.MAILGUN_DOMAIN
});

// The empty object '{}' passed to User crud methods is an optional options object to control the response received from database
// Right now since we are not using it, it's an empty object.

passport.serializeUser((user, done) => {
	console.log('Serializing user...');
	//console.log("user::"+JSON.stringify(user));
	return done(null, user);
});

passport.deserializeUser((user, done) => {
	console.log('Deserializing user...');
	const table=user.storeName+"_user_data";
	User.selectTable(table);
	User.getItem({
		email: user.email
	}, null, (err, user) => {
		if (err) {
			console.log('err: ', err);
			return done(err);
		} else {
			return done(null, user);
		}
	});
});

passport.use('local-signup', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, (req, email, password, done) => {
	const bodyParams = req.body; // here we extract the username and any other details passed by the user while registering
	const rootUrl = `${req.protocol}://${req.get('host')}`;
	const Url = req.get('host');
	const store=Url.split(".");
	const table=store[0]+"_user_data";
	if (bodyParams.password !== bodyParams.cpassword) {
		req.flash('error', 'Passwords don\'t match!');
		req.session.save(() => {
			return done(null, false);
		});
	} else {
		User.selectTable(table);
		User.getItem({
			email: email
		}, {}, (err, user) => {
			if (err) {
				console.log('err: ', err);
				req.flash('error', `${err.message}`);
				req.session.save(() => {
					return done(err);
				});
			} else if (Object.keys(user).length === 0) {
				// inserting data start from here
				const createCallback = (hashnewpwd) => {
					
					const verification_code = randomstring.generate();
					const putParams = {
						"email": email,
						"username": bodyParams.username,
						"password": hashnewpwd,
						"verification_code": verification_code,
						"verifiedornot": 'no',
					};
					console.log("Adding a new item...\n");

					User.createItem(putParams, {
						table: table
					}, (err, user) => {
						if (err) {
							req.flash('err', `${err.message}`);
							req.session.save(() => {
								return done(err);
							});
						} else {
							console.log('\nAdded user\n', user.email);
							const mailData = sendMail.sendVerificationMail(email, verification_code, rootUrl);
							mailgun.messages().send(mailData, (err, body) => {
								if (err) {
									console.log('err: ', err);
									req.flash('error', 'Failed!');
									req.session.save(() => {
										return done(err, false);
									});
								} else {
									console.log('Sent mail to your mailid successfully');
									req.flash('success', 'Registered! Verify your mailid');
									req.session.save(() => {
										user["storeName"]=store[0];
										return done(null, user);
									});
								}
							});
						}
					});

				};

				bcrypt.genSalt(10, (err, salt) => {
					if (err) {
						console.log(`\nmsg-1\n ${err.message}`);
						req.flash('error', `${err.message}`);
						req.session.save(() => {
							return done(err);
						});
					} else {
						bcrypt.hash(password, salt, (err, hashnewpwd) => {
							if (err) {
								console.log(`\nmsg-2\n ${err.message}`);
								req.flash('error', `${err.message}`)
								req.session.save(() => {
									return done(err);
								});
							} else {
								createCallback(hashnewpwd);
							}
						})
					}
				})

				// end here
			} else if (Object.keys(user).length > 0) {
				req.flash('error','Emailid already registered!');
				req.session.save(()=>{
					return done(null, false);
				});
			}
		});
	}
}));

passport.use('local-login', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, (req, email, password, done) => {
	//Dynamic storename setting
	const Url = req.get('host');
	const store=Url.split(".");
	const table=store[0]+"_user_data";

	process.nextTick(function() {
	//console.log("email: "+email);
	User.selectTable(table);
	User.getItem(email, {}, (err, user) => {
		//console.log('username: ', user.username);
		if (err) {
			req.flash('error', `${err.message}`);
			req.session.save(() => {
				return done(err);
			})
		} else if (Object.keys(user).length === 0) {
			return done(null, false, {
				message: 'Incorrect user!'
			});
		} else {
			const hashPassword = user.password;
			bcrypt.compare(password, hashPassword, (err, result) => {
				if (err) {
					console.log('err: ', err);
					req.flash('error', `${err.message}`);
					req.session.save(() => {
						return done(err)
					});
				}
				if (result) {
					console.log('\npassword matches', result);
					req.flash('success', 'Logged in successfully!');
					req.session.save(() => {
						user['storeName']=store[0];
						return done(null, user);
					});
				} else {
					req.flash('error', 'Incorrect password!');
					req.session.save(() => {
						return done(null, false);
					});
				}
			})
		}

	})
});
}));

module.exports = passport