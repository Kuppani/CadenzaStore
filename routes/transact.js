const router = require('express').Router();
const uuidv4 = require('uuid/v4');
const uniqid = require('uniqid');

const checksum = require('../models/paytm/checksum');
const Order = require('../models/order');
const liveConfig = require('../config/payments/live-config');
const testConfig = require('../config/payments/test-config');
const Methods = require('../methods/custom');
const middleware = require('../middleware/index');

const config = require('../config/config');
const Cart = require('../methods/cart');

const paymentFunction = (req, res, paymentEnv = 'test') => { //default to live mode
	console.log("POST Order start");

	const orderId=req.body.order_id;
	const total=req.body.total;
	//console.log("order_id: "+orderId);

	const basket=req.session.basket;
	//console.log("basket::",JSON.stringify(basket));
	const bodyParams = Methods.excludeProperties(req.body,[
		'first_name',
		'last_name',
		'company',
		'address',
		'city',
		'order_id',
		'state',
		'pincode',
		'phone',
		'total'
	]); //exclude address parameters from passing them to paytm
	//console.log("body params: "+bodyParams);
	const config = (paymentEnv === 'live') ? liveConfig : testConfig;

	let paramlist = config.defaultRequest();
	const paramObj = {};

	
		paramlist = config.subscribeRequest();
		paramlist.SUBS_SERVICE_ID = uniqid('subID');
		paramlist.SUBS_AMOUNT_TYPE = 'FIX';
		paramlist.SUBS_FREQUENCY = '1';
		paramlist.SUBS_FREQUENCY_UNIT = 'DAY';
		paramlist.SUBS_ENABLE_RETRY = '1'; //possible values are 0 or 1
		// paramlist.SUBS_EXPIRY_DATE = new Date().setFullYear(2018);
		paramlist.SUBS_EXPIRY_DATE = Methods.formatDate(new Date().setDate(new Date().getDate() + 7));
	
	paramlist.ORDER_ID = orderId
	paramlist.CUST_ID = uniqid('usr');
	paramlist.REQUEST_TYPE = 'SUBSCRIBE';
	// paramlist.CALLBACK_URL = `${req.protocol}://${req.get('host')}/response`; //This is for localhost testing
	paramlist.CALLBACK_URL = `https://${req.get('host')}/response`; //This is for EC2 
	
	paramlist.TXN_AMOUNT = total || 0;

	for (name in bodyParams) {
		if (bodyParams.hasOwnProperty(name)) {
			paramlist[name] = bodyParams[name];
		}
	}

	for (name in paramlist) {
		if (name == 'PAYTM_MERCHANT_KEY') {
		var PAYTM_MERCHANT_KEY = paramlist[name];
		} else if (name !== 'PAYTM_FINAL_URL') {
      		paramObj[name] = paramlist[name];
		}
	}
  //console.log('paramlist: ', paramlist);
  //console.log('paramObj: ', paramObj);

	checksum.genchecksum(paramObj, PAYTM_MERCHANT_KEY, (err, result) => { //generate checksum before passing it to payment
		console.log('\nGenerating checksum...');
		res.locals.restdata = result;
		res.locals.url = paramlist.PAYTM_FINAL_URL;
		if(total < 1) {
			req.flash('error',`Your cart is either empty or doesn't have minimum value`);
			Methods.customRedirect(req,res,'back');
		} else {
			res.render('pgredirect');
		}
	});
	console.log("POST Order end");
}

// payment page 
router.post('/checkout', middleware.isLoggedIn,(req, res) => {
		const bodyParams=req.body;
		console.log("billing details\n");
		//console.log(bodyParams);
		const putParams ={
			order_id:bodyParams.order_id,
			address:bodyParams
		}
		const Url = req.get('host');
	    const store=Url.split(".");
		const table=store[0]+"_order_data";
		
		console.log("put params: "+putParams);
		Order.selectTable(table);
		Order.createItem(putParams, {
			table: table
		}, (err, order) => {
			if (err) {
				console.log("error: "+err.message);
			}
			else{
				console.log("success..details:\n ");
				paymentFunction(req, res, config.ENVIRONMENT.toLowerCase());
			}
		});
});

module.exports = router;
