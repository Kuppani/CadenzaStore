const checksum = require('../models/paytm/checksum');
const testConfig = require('../config/payments/test-config');
const liveConfig = require('../config/payments/live-config');

const router = require('express').Router();
const middleware = require('../middleware/index');
const config = require('../config/config');
const Order = require('../models/order');
const sendMail = require('../methods/email');

const mailgun = require('mailgun-js')({
	apiKey: config.API_KEY_MAILGUN,
	domain: config.MAILGUN_DOMAIN
});

const paymentResponse = (req, res, paymentEnv = 'live') => {
	const Url = req.get('host');
	const store=Url.split(".");
	const table=store[0]+"_order_data";

	console.log("in response post");
	const paramlist = req.body;
	console.log('\n\n response parameters\n', paramlist);
	console.log('\nuser at response', req.user);
	res.locals.paramlist = paramlist;
	let cart = req.session.basket;
	cart.baskettype=req.session.baskettype;
	cart.orderId = paramlist.ORDERID;
	cart.status = paramlist.STATUS;
	cart.paymentMode = paramlist.PAYMENTMODE;
	//Response parameters
	//MID // STATUS //TXNID //ORDERID //TXN_AMOUNT //RESPCODE //RESPMSG //BANKNAME //PAYMENTMODE
	req.session.basket = {};

	const tempConfig = (paymentEnv === 'live') ?
		liveConfig.defaultRequest() :
		testConfig.defaultRequest();
	console.log('config.PAYTM_MERCHANT_KEY: ', tempConfig.PAYTM_MERCHANT_KEY);
	if (checksum.verifychecksum(paramlist, tempConfig.PAYTM_MERCHANT_KEY)) {
		console.log("true");
		const updateParams = {
			order_id: paramlist.ORDERID,
			successfulCart: cart
		}
		res.locals.restdata = true;
		Order.selectTable(table);
		Order.updateItem(updateParams, {}, (err, order) => {
			if (err) {
				console.log('\nerr\n', err.message);
			} else {
				console.log('\nsuccessfully update order');
				const orderId=paramlist.ORDERID;
				Order.getItem(orderId, {}, (err, order) => {
					if (err) {
						console.log('err: ', err);
						
					} else {
						//console.log("get success");
						res.locals.order=order;
						const mailData = sendMail.invoiceReport(order);
						mailgun.messages().send(mailData, (err, body) => {
							if (err) {
								console.log('err: ', err);
								//req.flash('error', 'Failed!');
							} else {
								console.log('Sent mail to your mailid successfully');
								//req.flash('success', 'Registered! Verify your mailid');
							}
						});
						res.render('response');
					}
				});
			}
		});
	} else {
		const updateParams = {
			order_id: paramlist.ORDERID,
			failedCart: cart
		}
		console.log("false");
		res.locals.restdata = false
		Order.selectTable(table);
		Order.updateItem(updateParams, {}, (err, order) => {
			if (err) {
				console.log('\nerr\n', err.message);
			} else {
				console.log('\nsuccessfully update order');
			}
			res.render('response');
		});
	}
	
}

router.post('/response', (req, res) => {
	paymentResponse(req, res, config.ENVIRONMENT.toLowerCase());
});


module.exports = router;