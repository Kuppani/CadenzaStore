const router = require('express').Router();

const uuidv4 = require('uuid/v4');
const Subscribe = require('../models/subscription');
const Storename = require('../models/storename');
const middleware = require('../middleware/index');

// Home page
router.get('/', (req, res) => {
	
	console.log(req.get('host'));
	const url=req.get('host');
	const store=url.split(".");
	const storeName=store[0];
	if(req.query.username){
		res.locals.currentUser=req.query.username;
		console.log("currentuser"+res.locals.currentUser);
	}
	console.log(store[0]);
	
	Storename.selectTable('store_details');
	
	Storename.getItem(storeName, {}, (err, name) => {
		console.log(name, err);
		if (err) {
			return res.send(statusMsg.errorResponse(err))
		} if (Object.keys(name).length === 0) {
			//console.log("storename not found");
			res.render("store");
		}
		if (Object.keys(name).length > 0) {
			// add a fallback in case there is an error fetching the product/plan data from the database
			const table=storeName+"_store_data";
			Subscribe.selectTable(table);
			Subscribe.query('basket', (err, baskets) => {
				const jsond=JSON.stringify(baskets);
				console.log("baskets:::"+jsond);
				res.locals.baskets = baskets || {};
				res.locals.basketCount = baskets.length || 0;
				res.render('index');
			});
		}
	});

});

// Plans page
router.get('/plans',(req,res) =>{

	const Url = req.get('host');
    const store=Url.split(".");
    const table=store[0]+"_store_data";
	
	const params={
        "collectionType": 'basket',
        "collectionId": req.query.id
	};
	Subscribe.selectTable(table);
	Subscribe.getItem(params, {}, (err, basket) => {
		if (err) {
			return res.send(statusMsg.errorResponse(err))
		}else{
			res.locals.basket = basket || {};
			const jsond=JSON.stringify(basket);
			console.log("basket:::"+jsond);			
			//res.render('plans');
			res.render("single-basket");
		}
	});

})


// Order summary page
router.get('/order',(req,res) =>{

	const Url = req.get('host');
    const store=Url.split(".");
    const table=store[0]+"_store_data";
	
	const params={
        "collectionType": 'basket',
        "collectionId": req.query.id
	};
	Subscribe.selectTable(table);
	Subscribe.getItem(params, {}, (err, basket) => {
		if (err) {
			return res.send(statusMsg.errorResponse(err))
		}else{
			res.locals.basket = basket || {};
			res.locals.plantype=req.query.type;
			req.session.basket=basket;
			req.session.baskettype=req.query.type;
			const jsond=JSON.stringify(basket);
			console.log("basket:::"+jsond);			
			res.render('order');
		}
	});

})

// Checkout page
router.post('/check',(req,res) =>{
	const orderId=uuidv4();
	res.locals.total=req.body.total;
	res.locals.orderId=orderId;
	res.render("checkout");

})


router.get('/profile', middleware.isLoggedIn, (req, res, next) => {
	res.render('profile');
});

module.exports = router;
