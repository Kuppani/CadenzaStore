// Mailgun mail content
const mailTestLink = 'https://www.w3schools.com/test/names.asp';
const fromMail = 'dev@predator-digital.com';

const sendVerificationMail = (userMail, verification_code, rootUrl) => {
	const tempUrl = `${rootUrl || mailTestLink}/activateaccount`;
	// The usermail id's should be later replaced with tokens that can be used to
	// identify the users

		//<p>Please use the below link to verify your email address 
	//	${tempUrl}?emailid=${userMail}&token=${verification_code}</p>
	return {
		from: `Store Owner <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Verification mail',
	
		html: `

		<!doctype html>
		<html lang="en">
		   <head>
			  <meta charset="utf-8" />
			  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
			  <!-- <link rel="apple-touch-icon" sizes="76x76" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/apple-icon.png" /> -->
			  <link rel="icon" type="image/png" href="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/favicon.png" />
			  <title>Verify</title>
			  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
			  <meta name="viewport" content="width=device-width" />
			  <!-- CSS Files -->
			  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			  <link href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/css/styles.css" rel="stylesheet" />
			  <!-- Fonts and Icons -->
			   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
		
			  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
		
		
		   </head>
		   <body style="color: #66615b;
		   font-size: 14px;
		   font-family: 'Muli', Arial, sans-serif;">
			  <div class="image-container set-full-height" style=" min-height: 100vh;
			  background-position: center center;
			  background-size: cover;
			  background-image: url('https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/bg.png');">
				 <div class="header" style="float:left; width: 100%;">
					<div class="logosection" style="width: 14%;margin: 40px 36% 0px;">         
					   <a href="#"><img src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/new_logo.png"></a>
					</div>
				 </div>
				 <!--   Big container   -->
				 <div class="container" style="margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;box-sizing: border-box;">
		
					<div class="row" style="margin-left:-15px;margin-right:-15px">
					   <div class="col-sm-4 col-sm-offset-4" style="margin-left:33.33333333%;width:33.33333333%;">
						  <!--      WizardTables container        -->
						  <div class="wizard-container" style="z-index: 3;" >
						  <div class="wizard-header1">
						  <div class="signuphead" style=" float: right; 
						  width: 25%;
						  background: #2e3442;
						  color: #fff;
						  text-align: center;
						  padding: 5px 0%;
						  border: 1px solid #2e3442;
						  border-radius: 10px 10px 0px 0px;
						  font-size: 13px;
						  font-weight: bolder;">Verify</div>
					   </div>
							 <div class="signupbody" style="float: left; width: 100%; border-top: 5px solid #2e3442; border-radius: 10px 0px 0px 0px;">
								<div class="card wizard-card" data-color="green" style=" min-height: 350px;
								box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57);  border-radius: 6px;
								box-shadow: 0 2px 2px rgba(204, 197, 185, 0.5);
								background-color: #fafafa;
								color: #252422;
								padding: 10px 0;
								margin-bottom: 20px;
								position: relative;
								z-index: 1;" >
								   <form action="">
									  <div class="row" style="margin-left:-15px;margin-right:-15px">
										 <div class="col-sm-10 col-sm-offset-1"style="width:83.33333333%;margin-left:8.33333333%">
											<div class="col-sm-12" style="float:left; width:100%">
											   <div class="wizard-header1">
												  <div class="usericon" style="width: 15%;
												  margin: auto;"><img alt="verify" src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/verify.png"></div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">Thanks for choosing our service</div>
												  </div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">To continue to login</div>
												  </div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">Click to verify your email id</div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <a href="${tempUrl}?emailid=${userMail}&token=${verification_code}"><input type="submit" name="submit" class="submitlink" style="float: left;
												  background-color: #2e3442;
												  border: #2e3442;
												  border-radius: 5px;
												  width: 50%;
												  text-align: center;
												  padding: 8px 0%;
												  color: #FFFFFF;
												  font-weight: bold;
												  margin: 30px 25%;" value="Verify"></a>
											   </div>
											</div>
										 </div>
									  </div>
								   </form>
								</div>
							 </div>
						  </div>
						  <!-- wizard container -->
					   </div>
					</div>
					<!-- row -->
				 </div>
				 <!--  big container -->
			  </div>
		   </body>
		   <!--   Core JS Files   -->
		   <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>  
		   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
		   <!--  Plugin for the Wizard -->
		   <script src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/js/paper-bootstrap-wizard.js" type="text/javascript"></script>  
		   <script src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/js/jquery.validate.min.js" type="text/javascript"></script>
		   <script>
			  $(document).ready(function(){            
				  //$("#basketone").show();
				  $("#baskettwo").show();
			  
				  //$("#planone").show();
				  $("#plantwo").show();    
			  
				  //$("#paymentone").show();
				  $("#paymenttwo").show(); 
			  });
		   </script>   
		</html>

		`
	};
};

const forgotPasswordMail = (userMail, encryptedMail, rootUrl) => {
	// The rootUrl is temporary (for now it gives the public DNS of ec2 server)
	///${rootUrl}/resetpassword?token=${encryptedMail}

	return {
		from: `Store owner <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Forgot password',
		
		html: `
		<!doctype html>
        <html lang="en">
           <head>
              <meta charset="utf-8" />
              <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
              <!-- <link rel="apple-touch-icon" sizes="76x76" href="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/apple-icon.png" /> -->
              <link rel="icon" type="image/png" href="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/favicon.png" />
              <title>Verify</title>
              <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
              <meta name="viewport" content="width=device-width" />
              <!-- CSS Files -->
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <link href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/css/styles.css" rel="stylesheet" />
              <!-- Fonts and Icons -->
               <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
        
               <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
        
        
           </head>
           <body style="color: #66615b;
           font-size: 24px;
           font-family: 'Ubuntu">
              <div class="image-container set-full-height" style=" min-height: 100vh;
              background-position: center center;
              background-size: cover;
              background-image: url('https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/bg.png');">
                 <div class="header" style="float:left; width: 100%;">
                    <div class="logosection" style="width: 14%;margin: 40px 36% 0px;">          
                       <a href="#"><img src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/new_logo.png"></a>
                    </div>
                 </div>
                 <!--   Big container   -->
                 <div class="container" style="margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;box-sizing: border-box;">
        
                    <div class="row" style="margin-left:-15px;margin-right:-15px">
                       <div class="col-sm-4 col-sm-offset-4" style="margin-left:33.33333333%;width:33.33333333%;">
                          <!--      WizardTables container        -->
                          <div class="wizard-container" style="z-index: 3;" >
                          <div class="wizard-header1">
                          <div class="signuphead" style=" float: right; 
                          width: 25%;
                          background: #2e3442;
                          color: #fff;
                          text-align: center;
                          padding: 5px 0%;
                          border: 1px solid #2e3442;
                          border-radius: 10px 10px 0px 0px;
                          font-size: 13px;
                          font-weight: bolder;">Forgot Password</div>
                       </div>
                             <div class="signupbody" style="float: left; width: 100%; border-top: 5px solid #2e3442; border-radius: 10px 0px 0px 0px;">
                                <div class="card wizard-card" data-color="green" style=" min-height: 350px;
                                box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57);  border-radius: 6px;
                                box-shadow: 0 2px 2px rgba(204, 197, 185, 0.5);
                                background-color: #fafafa;
                                color: #252422;
                                padding: 10px 0;
                                margin-bottom: 20px;
                                position: relative;
                                z-index: 1;" >
                                   <form action="">
                                      <div class="row" style="margin-left:-15px;margin-right:-15px">
                                         <div class="col-sm-10 col-sm-offset-1"style="width:83.33333333%;margin-left:8.33333333%">
                                            <div class="col-sm-12" style="float:left; width:100%">
                                               <div class="wizard-header1">
                                                  <div class="usericon" style="width: 15%;
                                                  margin: auto;"><img alt="verify" src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/img/verify.png"></div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">Thanks for choosing our service</div>
                                                  </div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">We are here to help you</div>
                                                  </div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">Click below button to change password</div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <a href="${rootUrl}/resetpassword?token=${encryptedMail}"><input type="submit" name="submit" class="submitlink" style="float: left;
                                                  background-color: #2e3442;
                                                  border: #2e3442;
                                                  border-radius: 5px;
                                                  width: 50%;
                                                  text-align: center;
                                                  padding: 8px 0%;
                                                  color: #FFFFFF;
                                                  font-weight: bold;
                                                  margin: 30px 25%;" value="Change Password"></a>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                   </form>
                                </div>
                             </div>
                          </div>
                          <!-- wizard container -->
                       </div>
                    </div>
                    <!-- row -->
                 </div>
                 <!--  big container -->
              </div>
           </body>
           <!--   Core JS Files   -->
           <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>   
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
           <!--  Plugin for the Wizard -->
           <script src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/js/paper-bootstrap-wizard.js" type="text/javascript"></script>   
           <script src="https://s3.ap-south-1.amazonaws.com/cadence-assets/assets1/js/jquery.validate.min.js" type="text/javascript"></script>
           <script>
              $(document).ready(function(){         
                  //$("#basketone").show();
                  $("#baskettwo").show();
              
                  //$("#planone").show();
                  $("#plantwo").show(); 
              
                  //$("#paymentone").show();
                  $("#paymenttwo").show();  
              });
           </script>    
        </html>
		
		`
	};
};

const invoiceReport = (order) => {
	const plans =order['successfulCart'].plans['silver'];
	const plantype=order['successfulCart'].baskettype;
	const reOrder=order['successfulCart'].plans[plantype].reOrder;

	const discount=order['successfulCart'].plans[plantype].discount;
	var srp=order.successfulCart.srp;
	var month=(srp * discount)/100;
	var gcost=srp-month;
	var tot=order['successfulCart'].plans[plantype].reOrder * gcost; 
	var total=tot.toFixed(2);
	return {
		from: `Cadence-commerce.com <${fromMail}>`,
		to: `p.kuppani@predator-digital.com`,
		subject: `Your Cadence-commerce.com order of ${order['successfulCart'].title}`,
		
		html: `<div class="image-container set-full-height">

	    <!-- Invoice Starts here-->
	    <div class="container">
	        <div class="row">
	        	<div class="invoice" style="width:80%; margin:0px auto; ">

		        	<div class="invoicestart" style="width:100%;float:left;padding: 0px 3%; border:1px solid #ccc; margin:40px 0%;">
			        	<div class="titlehead" style="float: left;width:100%; text-align:right; font-weight:bold;
			        	font-size:22px; color:#000; padding:40px 0%;">Cadenza</div>

			        	<div class="addressdets" style="width:100%;float: left;">
			        		<div class="addresshead" style="border:1px solid #ccc; padding:5px 1%; 
			        		width:100%;float: left;">
			        			<div class="invoicetitle" style="float: left;width:45%; color:#000; font-size:26px;
			        			text-align:left; font-weight:bold;">INVOICE</div>
			        			<div class="orderno" style="float: left;background-color:#3fa9f5;color:#fff; font-weight:bold; font-size:14px; width:20%;    padding: 8px 0%;text-align: center;">ORDER NO: ${order['order_id']}</div>
			        			<div class="orderdate" style="width: 33%;float: left;background-color: green;color: #fff;font-weight: bold;font-size: 14px;margin: 0px 0% 0px 2%;text-align: center;padding: 8px 0%;">ORDER STATUS: ${order['successfulCart'].status}</div>
			        		</div>

			        		<div class="addressinfo" style="width:100%; float:left; margin:30px 0%;">
			        			<div class="shippinginfo" style="width:33.33%; float:left;">
			        				<div class="shippinginfohead" style="width:100%; float:left;font-weight:bold; font-size:16px; color:#000; padding:10px 0%;">SHIPPING DETAILS</div>
			        				<div class="shippingaddress" style="width:100%; float:left;">
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].first_name}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].city}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].state}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].phone}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].pincode}</p>
			        					<!-- <p style="width:100%;float:left;color:#000;font-size:14px;">+12122</p> -->
			        				</div>
			        			</div>
								<% var srp=order.order_id; %>
			        			<div class="shippinginfo" style="width:33.33%; float:left;">
			        				<div class="shippinginfohead" style="width:100%; float:left;font-weight:bold; font-size:16px; color:#000; padding:10px 0%;">BILLING DETAILS</div>
			        				<div class="shippingaddress" style="width:100%; float:left;">
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].first_name}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].city}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].state}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].phone}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].pincode}</p>
			        				</div>
			        			</div>
			        			<div class="shippinginfo" style="width:33.33%; float:left;">
			        				<div class="shippinginfohead" style="width:100%; float:left;font-weight:bold; font-size:16px; color:#000; padding:10px 0%;">CUSTOMER DETAILS</div>
			        				<div class="shippingaddress" style="width:100%; float:left;">
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].first_name}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].EMAIL}</p>
			        					<p style="width:100%;float:left;color:#000;font-size:14px;">${order['address'].phone}</p>
			        				</div>
			        			</div>
			        		</div>


			        		<div class="orderfullcheckout" style="float:left;width:100%;">
			        			<div class="orderfullcheckouthead" style="float:left;width:100%; background-color:#3fa9f5; border:1px solid #ccc;padding:5px 0%; text-align:center; color:#fff; font-weight:bold; font-size:16px">
			        				<div class="in_title" style="float:left;width:30%;">Basket</div>
			        				<div class="in_vender" style="float:left;width:25%;">Plan</div>
			        				<div class="in_city" style="float:left;width:10%;">Price/month</div>
			        				<div class="in_tax" style="float:left;width:10%;">Time period</div>
			        				<div class="in_unitprice" style="float:left;width:15%;">Total</div>
			        
			        			</div>

								<!-- Product -->
								
			        			<div class="orderfullcheckoutinfo" style="border:1px solid #ccc;padding:1%; text-align:center;font-size:16px; float:left; width:100%; margin:5px 0% 0px;">			        				
			        				<div class="in_title" style="float:left;width:30%;">
			        					<div class="tileimg" style="float:left;width:20%;"><img src="${order['successfulCart'].imageURL}" alt="image" style="float:left; width:100%;height:auto;border: 1px solid #ccc; padding:1%;"></div>
			        					<div class="titlname" style="float:left;width:80%; color:#3fa9f5;">${order['successfulCart'].title}</div>
			        				</div>
			        				<div class="in_vender" style="float:left;width:25%;">${order['successfulCart'].baskettype}</div>
			        				<div class="in_city" style="float:left;width:10%;">${gcost}</div>
			        				<div class="in_tax" style="float:left;width:10%;">${reOrder} Months</div>
			        				<div class="in_price" style="float:left;width:10%;">${total}</div>
			        			</div>
			
			        		</div>



			        		<div class="shippinginfo" style="width:100%; float:left; margin-top:30px;">
			        				<div class="shippingaddress" style="width:100%; float:left;">
			        					<p style="width:87%;float:left;color:#000;font-size:16px;text-align:right;margin-right:2%;">DISCOUNT:</p>
			        					<p style="width:8%;float:left;color:#000;font-size:16px;text-align:left;">0</p>
			        					<p style="width:87%;float:left;color:#000;font-size:16px;text-align:right;margin-right:2%;">SUB TOTAL:</p>
			        					<p style="width:8%;float:left;color:#000;font-size:16px;text-align:left;">0</p>
			        					<p style="width:87%;float:left;color:#000;font-size:16px;text-align:right;margin-right:2%;">SHIPPING:</p>
			        					<p style="width:8%;float:left;color:#000;font-size:16px;text-align:left;">0</p>
			        					<p style="width:87%;float:left;color:#000;font-size:16px;text-align:right;margin-right:2%;">TAX:</p>
			        					<p style="width:8%;float:left;color:#000;font-size:16px;text-align:left;">0</p>
			        				</div>

			        				<div class="addresshead" style="border:1px solid #3fa9f5; padding:5px 1%; width:100%;float: left;">

			        					<p style="width:88%;float:left;color:#000;font-weight:bold;font-size:16px;text-align:left;margin-right:2%;">TOTAL PAID:</p>
			        					<p style="width:8%;float:left;color:#000;font-weight:bold;font-size:16px;text-align:left;">${total}</p>
			        					
			        				</div>


			        			</div>

			        			<div class="footerinfo" style="float:left; width:100%; margin:40px 0%;">
			        				<p style="color:#838383">Thank you for your valued business.</p>
			        				<p style="color:#838383">We garently value your trust and confidence and sincerely appreciate your loyalty to our business.</p>
								</div>
			        	</div>
			        </div>

			    </div>
	        </div>	
	    </div>
	    <!-- Invoice Ends here -->
	    
	</div>`
	};
};

module.exports = {
	sendVerificationMail,
	forgotPasswordMail,
	invoiceReport
}

	
