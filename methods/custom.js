// Sets the table to required table and then returns the body parameters
const initializer = (req, Model) => {
    const reqBody = Object.assign({},req.body);
    const table = reqBody.table;
    if (table) {
        console.log('table: ', table);
        Model.selectTable(table);
	};
    return excludeProperties(reqBody,['table']);
};

const excludeProperties = (obj, keysArr)=> {
    console.log('obj: ', obj);
    const target = new Object();
    for (let prop in obj) {
        if (keysArr.indexOf(prop) >= 0) continue;
        if (!obj.hasOwnProperty(prop)) continue;
        target[prop] = obj[prop];
    }
    // console.log('\nTrimmed Object\n',target);
    return target;
};

const apiOperation = (req, res, Model, crudMethod)=> {
    const bodyParams = initializer(req, Method);
    crudMethod(bodyParams, (err, result) => {
        console.log('\ncallback name', crudMethod.name);
        console.log('\ndata\n', result);
        res.send(result);
    });
};
;
const formatDate = function (date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};

const customRedirect = (req,res,redirectPage='/')=>{
    //The page redirect defaults to '/', the root page
    //This is used to save any unsaved data to the session manually before redirecting thereby avoiding the racearound condition where the session data is set after redirect forcing us to reload the page;
	req.session.save(()=>{
        return res.redirect(`${redirectPage}`);
    });
}

module.exports = {
    initializer,
    excludeProperties,
    apiOperation,
    formatDate,
    customRedirect,
}