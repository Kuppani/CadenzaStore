const dotenv =require('dotenv').config();

const express = require('express');
const app = express();
const router = express.Router();
const flash = require('connect-flash');

const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');
const session = require('express-session');
const methodOverride = require('method-override');
const DynamoDBStore = require('dynamodb-store');
const passport = require('passport');
const config = require('./config/config');
const Cart = require('./methods/cart');

const minute = 60000;
const hour = 3600000;
const day = 86400000;

const options = {
    "table": {
        "name":"test-sessions",
        "hashPrefix": '',
    },
    "dynamoConfig": {
        "accessKeyId": config.ACCESS_KEY_ID,
        "secretAccessKey": config.SECRET_ACCESS_KEY,
        "region": 'ap-south-1',
    },
    "keepExpired": false,
};

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));

// view engine setup
app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'views/pages')]);
app.set('view engine', 'ejs');
//Flash messages
app.use(flash());

// The order of app.use middleware matters
//Configuration of the order matters; passport and flash messages have to be declared after session middleware

app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride('_method'));
app.use(session({ //will work with everything below this so declare your static assets above
    store: new DynamoDBStore(options),
    secret: config.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: true, //only allows cookie access to server
        maxAge: hour
    }
}));
//passport config
app.use(passport.initialize());
app.use(passport.session()); //At this point of time flash messages are not working; Have to revisit this later for debugging

//Custom Middleware
app.use((req, res, next) => {
    res.locals.currentUser = (req.user) ? req.user.username : undefined;
    res.locals.success = req.flash('success');
    res.locals.error = req.flash('error');
    
    //console.log('\nThe session is\n',req.session);
    //console.log('\nThe locals object is\n',res.locals);
    next();
});

app.use('/', router); //to begin using the router

//Core Routes
const index = require('./routes/index');

//login routes
const authentication = require('./routes/authenticate');
//payment routes
const transact = require('./routes/transact');
const response = require('./routes/response');
//error handling
const errorHandler = require('./routes/error');


app.use('/',index);
//app.use('/',cart);
app.use('/',authentication);
app.use('/',transact);
app.use('/',response);
app.use('/',errorHandler);

module.exports = app;
