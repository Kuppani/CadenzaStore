const Joi = require('joi');
const config = require('../config/config');
const SchemaModel = require('../config/schema');
//const tableName = 'keeros-orders';

const attToGet = ['address','successfulCart','failedCart'];
const attToQuery = ['address','successfulCart','failedCart','orderId'];

const orderSchema = {
    hashKey: 'order_id',
    //rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        order_id: Joi.string(),
        address:Joi.object(),
        successfulCart:Joi.object(),
        failedCart: Joi.object()
    }).optionalKeys(attToGet).unknown(true)
};

const optionsObj  = {
    attToGet,
    attToQuery
    //tableName
};
const Order = SchemaModel(orderSchema,optionsObj);

module.exports = Order;